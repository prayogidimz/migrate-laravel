<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

 //cast
 Route::prefix('cast')->group(function () {
    Route::get('/', [CastController::class, 'index'])->name('cast.index');
    Route::get('/create', [CastController::class, 'create'])->name('cast.create');
    Route::post('/store', [CastController::class, 'store'])->name('cast.store');
    Route::get('/edit/{id}', [CastController::class, 'edit'])->name('cast.edit');
    Route::post('/update/{id}', [CastController::class, 'update'])->name('cast.update');
    Route::get('/destroy/{id}', [CastController::class, 'destroy'])->name('cast.destroy');
    Route::get('/show/{id}', [CastController::class, 'show'])->name('cast.show');
});

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{
    public function index(){
        $data = Cast::get();

        return view('cast.index',compact('data'));
    }

    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $data               = New Cast();
        $data->nama     = $request->input('nama');
        $data->umur   = $request->input('umur');
        $data->bio   = $request->input('bio');
        $data->save();

        return redirect(route('cast.index'))
                    ->with('success', 'Data berhasil disimpan');
    }

    public function edit(Request $request){
        $data = Cast::find($request->id);
        
        return view('cast.edit',compact('data'));
    }

    public function update(Request $request){
        $data               = Cast::find($request->id);
        $data->nama     = $request->input('nama');
        $data->umur   = $request->input('umur');
        $data->bio   = $request->input('bio');
        $data->save();

        return redirect(route('cast.index'))
                    ->with('success', 'Data berhasil disimpan');
    }

    public function destroy(Request $request){
        $data               = Cast::find($request->id);

       
        $data->delete();

        return redirect(route('cast.index'))
                    ->with('success', 'Data berhasil dihapus');
    }

    public function show(Request $request){
        $data = Cast::find($request->id);
        
        return view('cast.show',compact('data'));
    }


}

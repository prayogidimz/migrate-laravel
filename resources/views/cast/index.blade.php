@extends('layout.master')

@section('content')
  <div class="container">
    <h3>Cast Table</h3>
    <table class="table table-bordered">
        <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Umur</th>
              <th>Bio</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
          @foreach($data as $key => $b)
            <tr>
              <td>{{ $key+1 }}.</td>
              <td>{{ $b->nama }}</td>
              <td>{{ $b->umur }}</td>
              <td>{{ $b->bio }}</td>
              <td>
                <a href="{{ route('cast.edit',$b->id) }}" class="btn btn-warning">Edit</a>
                <a href="{{ route('cast.destroy',$b->id) }}" class="btn btn-danger">Delete</a>
                <a href="{{ route('cast.show',$b->id) }}" class="btn btn-secondary">Show</a>
              </td>
            </tr>
          @endforeach
        </tbody>
    </table>
    <a href="{{ route('cast.create') }}" class="btn btn-primary">Tambah Data</a>
  </div>
@endsection
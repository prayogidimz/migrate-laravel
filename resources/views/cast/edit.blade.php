@extends('layout.master')

@section('content')
    
  <div class="container">
<h3> Ubah Data</h3>
   

    <form action="{{ route('cast.update',$data->id) }}" method="post">
    @csrf
    <div class="col-md-12">
            <div class="form-grup">
                <div class="col-md-6">
                    <label for="">Name</label>
                    <input type="text" name="nama" class="form-control" value="{{ $data->nama}}"><br>
                </div>
            </div>

            <div class="form-grup">
                <div class="col-md-6">
                    <label for="">Umur</label>
                    <input type="text" name="umur" class="form-control" value="{{ $data->umur}}"><br>
                </div>
            </div>

            <div class="form-grup">
                <div class="col-md-6">
                    <label for="">Bio</label>
                    <input type="text" name="bio" class="form-control" value="{{ $data->bio}}">
                </div>
            </div>
            <br>
            <div class="form-grup">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="{{ route('cast.index') }}" class="btn btn-secondary"> Back</a>
                </div>
            </div>
    </div>

                    
   </form>
   
   
  </div>
@endsection
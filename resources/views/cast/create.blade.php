@extends('layout.master')

@section('content')
    
  <div class="container-fluid">
     <h3>Buat Data</h3>
   
    <form action="{{ route('cast.store') }}" method="post">
    @csrf
       <div class="col-md-12">
            <div class="form-grup">
                <div class="col-md-6">
                    <label for="">Name</label>
                    <input type="text" class="form-control" name="nama">
                </div>
            </div>

            <div class="form-grup">
                <div class="col-md-6">
                    <label for="">Umur</label>
                    <input type="text" class="form-control" name="umur">
                </div>
            </div>

            <div class="form-grup">
                <div class="col-md-6">
                    <label for="">Bio</label>
                    <input type="text" class="form-control" name="bio">
                </div>
            </div> 
            
            <br>
            <div class="form-group">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="{{ route('cast.index') }}" class="btn btn-secondary"> Back</a>
                </div>
           </div>
        </div>
       
   </form>
   
   
  </div>
@endsection